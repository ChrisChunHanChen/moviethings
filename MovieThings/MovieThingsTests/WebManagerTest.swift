//
//  WebManagerTest.swift
//  MovieThingsTests
//
//  Created by Chris Chen on 2019/3/18.
//  Copyright © 2019 Dev4App. All rights reserved.
//

import XCTest
import Alamofire
import Alamofire_Gloss
import Gloss

@testable import MovieThings

class WebManagerTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testLoadMovieList(withPageNum pageNum: Int) {
        
        let expectations = expectation(description: "The Response result match the expected results")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let today = dateFormatter.string(from: Date())
        
        let urlString = "https://api.themoviedb.org/3/discover/movie?api_key=\(APIKey)&region=TW%7CCN&sort_by=release_date.desc&include_adult=false&include_video=false&page=\(pageNum)&release_date.lte=\(today)"
        
        if let requestUrl = URL(string: urlString) {
            
            let request = Alamofire.request(requestUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            request.responseObject(MovieList.self) { (response) in
                switch response.result {
                case .success(_):
                    // finally fullfill the expectation
                    expectations.fulfill()
                case .failure(_):
                    // failed case
                    expectations.fulfill()
                }
            }
            
            // wait for some time for the expectation (you can wait here more than 30 sec, depending on the time for the response)
            waitForExpectations(timeout: 30, handler: { (error) in
                if let error = error {
                    print("Failed : \(error.localizedDescription)")
                }

            })
        }
    }
    
    func testFetchMovieInfo(withMovieID movieID: Int) {
        XCTAssert(true)
    }
    
    func testLoadImage() {
        XCTAssert(true)
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.

        self.measure {
            // Put the code you want to measure the time of here.
            for i in 1...5 {
                testLoadMovieList(withPageNum: i)
            }
        }
    }

}
