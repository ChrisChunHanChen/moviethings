//
//  MovieListViewControllerTest.swift
//  MovieThingsTests
//
//  Created by Chris Chen on 2019/3/18.
//  Copyright © 2019 Dev4App. All rights reserved.
//

import XCTest
@testable import MovieThings


class MovieListViewControllerTest: XCTestCase {

    override func setUp() {

        _ = MovieListViewController()

    }
    
    override func tearDown() {
        
    }
    
    func testViewDidLoad() {
        XCTAssert(true)
    }

    func testLoadData() {
        XCTAssert(true)
    }
    
    func testRefreshTriggered() {
        XCTAssert(true)
    }

    func testExample() {
        testViewDidLoad()
        testLoadData()
        testRefreshTriggered()
    }

    func testPerformanceExample() {
        self.measure {
        
        }
    }

}
