//
//  MovieListViewController.swift
//  MovieThings
//
//  Created by Chris Chen on 2019/3/17.
//  Copyright © 2019 Dev4App. All rights reserved.
//

import UIKit
import SnapKit
import SVProgressHUD

class MovieListViewController: UIViewController {
    
    var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    
    private let movieCell = "MovieCollectionViewCell"
    
    var moviePage = 1
    var movieList: [MovieInfo] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // navigationController
        let title = "Movie Things"
        navigationItem.title = title
        
        // Layout for CollectionView
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 2, bottom: 2, right: 2)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let collectionViewWidth = Int(view.frame.width) - 5
        var collectionViewHeight = Int(view.frame.height) - ( Int(navigationController?.navigationBar.frame.height ?? 0) + 4)
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top
            let bottomPadding = window?.safeAreaInsets.bottom
            collectionViewHeight = collectionViewHeight - Int(topPadding ?? 0) - Int(bottomPadding ?? 0)
        }
        
        layout.itemSize = CGSize(width: Int(collectionViewWidth) / 2, height: Int(collectionViewHeight) / 2)
        
        collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(MovieCollectionViewCell.self, forCellWithReuseIdentifier: movieCell)
        
        collectionView.backgroundColor = UIColor.theme.lightGray
        view.addSubview(collectionView)
        
        let refreshController = UIRefreshControl()
        refreshController.addTarget(self,
                                    action: #selector(refreshTriggered(_:)),
                                    for: .valueChanged)
        collectionView.refreshControl = refreshController
        
        collectionView.snp.makeConstraints { (make) in
            if #available(iOS 11, *) {
                make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
                make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
                make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading)
                make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailing)
            } else {
                make.top.equalToSuperview()
                make.bottom.equalToSuperview()
                make.leading.equalToSuperview()
                make.trailing.equalToSuperview()
            }
        }
        
        loadData()
    }
    
    func loadData() {
        SVProgressHUD.show(withStatus: "Loading..")
        
        WebManager.loadMovieList(withPageNum: self.moviePage,
                                 failure: { (error) in
                                    print(error as Any)
        }, completion: { (objects) in
            SVProgressHUD.dismiss(withDelay: 0.3)
            self.movieList.append(contentsOf: objects.results)
            self.collectionView.reloadData()
        })
    }
    
    
    @objc func refreshTriggered(_ sender: UIRefreshControl) {
        
        moviePage = 1
        collectionView.refreshControl?.endRefreshing()
        loadData()
    }
}


extension MovieListViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: movieCell, for: indexPath) as? MovieCollectionViewCell {
            
            let movie = movieList[indexPath.row]
            cell.show(withMovieInfo: movie)
            
            WebManager.loadImage(forMovie: movie,
                                 failure: { (image) in
                                    if let image = image {
                                        cell.posterImageView.image = image
                                        movie.image = image
                                    }
            }, completion: { (image) in
                cell.posterImageView.image = image
                movie.image = image
            })
            
            if let lastMovie = movieList.last {
                let lastMovieReached = (movie.id == lastMovie.id)
                if (lastMovieReached && indexPath.row == movieList.count - 1) {
                    moviePage += 1
                    loadData()
                }
            }
            
            cell.show(withMovieInfo: movie)
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movie = movieList[indexPath.row]
        
        let viewController = MovieDetailsViewController()
        viewController.movie = movie
        navigationController?.pushViewController(viewController, animated: true)
    }
}

