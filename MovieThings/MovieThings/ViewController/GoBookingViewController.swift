//
//  GoBookingViewController.swift
//  MovieThings
//
//  Created by Chris Chen on 2019/3/17.
//  Copyright © 2019 Dev4App. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD

class GoBookingViewController: UIViewController {
    
    var webView = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // navigationController
        let title = "Go Booking"
        navigationItem.title = title
        
        let dismissImage = UIImage(named: "BarButtonItem-Cancel")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: dismissImage,
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(cancelButtonTapped))
        
        webView = WKWebView(frame: view.bounds)
        
        let urlString = "http://www.cathaycineplexes.com.sg/"
        let url = URL(string: urlString)
        let request = URLRequest(url: url!)
        
        // init and load request in webview.
        webView = WKWebView(frame: view.frame)
        webView.navigationDelegate = self
        webView.load(request)
        self.view.addSubview(webView)
        SVProgressHUD.show(withStatus: "Loading..")
    }
    
    @objc func cancelButtonTapped() {
        SVProgressHUD.dismiss()
        dismiss(animated: true, completion: nil)
    }
}

extension GoBookingViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SVProgressHUD.dismiss()
    }
}
