//
//  MovieDetailsViewController.swift
//  MovieThings
//
//  Created by Chris Chen on 2019/3/17.
//  Copyright © 2019 Dev4App. All rights reserved.
//

import UIKit
import SnapKit
import SVProgressHUD

class MovieDetailsViewController: UIViewController {
    
    var movieDetailsView = MovieDetailsView()
    var footerView = UIView()
    
    var movie: MovieInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // navigationController
        let title = "Movie Things"
        navigationItem.title = title
        
        let backImage = UIImage(named: "BarButtonItem-Back")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage,
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(backButtonTapped))
        
        view.backgroundColor = UIColor.theme.lightGray
        movieDetailsView = MovieDetailsView(frame: view.bounds)
        view.addSubview(movieDetailsView)
        
        movieDetailsView.snp.makeConstraints({ (make) in
            if #available(iOS 11, *) {
                make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
                make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading)
                make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailing)
            } else {
                make.top.equalToSuperview()
                make.leading.equalToSuperview()
                make.trailing.equalToSuperview()
            }
        })
        
        view.addSubview(footerView)
        
        footerView.snp.makeConstraints { (make) in
            make.height.equalTo(45)
            make.top.equalTo(movieDetailsView.snp.bottom)
            
            if #available(iOS 11, *) {
                make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
                make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading)
                make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailing)
            } else {
                make.bottom.equalToSuperview()
                make.leading.equalToSuperview()
                make.trailing.equalToSuperview()
            }
        }
        
        addFooterView()
        
        guard
            let movie = movie,
            let _ = movie.runtime else {
                loadData()
                return
        }
        
        movieDetailsView.show(withMovieInfo: movie)
    }
    
    // MARK: - helper functions
    
    @objc func backButtonTapped() {
        navigationController?.popViewController(animated: true)
    }
    
    func addFooterView() {
        let bookingButton = UIButton()
        let buttonImage = UIImage(named: "Button-Green")
        bookingButton.setTitle("Go Booking", for: .normal)
        bookingButton.setBackgroundImage(buttonImage, for: .normal)
        bookingButton.setTitleColor(UIColor.theme.darkGreen, for: .normal)
        bookingButton.setTitleColor(.white, for: .highlighted)
        bookingButton.layer.cornerRadius = 20
        bookingButton.clipsToBounds = true
        bookingButton.addTarget(self,
                                action: #selector(bookingButtonTapped(_:)),
                                for: .touchUpInside)
        footerView.addSubview(bookingButton)
        
        // bookingButton
        bookingButton.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalTo(150)
            make.centerX.equalTo(footerView.snp.centerX)
            make.centerY.equalTo(footerView.snp.centerY)
        }
    }
    
    func loadData() {
        
        SVProgressHUD.show(withStatus: "Loading..")
        guard
            let movie = movie,
            let movieID = movie.id else {
                return
        }
        
        WebManager.fetchMovieInfo(withMovieID: movieID,
                                  failure: { (error) in
                                    print(error)
        }, completion: { (movieInfo) in
            self.movie?.runtime = movieInfo.runtime
            self.movie?.genres = movieInfo.genres
            self.movieDetailsView.show(withMovieInfo: self.movie!)
            SVProgressHUD.dismiss(withDelay: 0.5)
        })
    }
    
    @objc func bookingButtonTapped(_ sender: UIButton) {
        let viewController = GoBookingViewController()
        let navigationController = UINavigationController(rootViewController: viewController)
        present(navigationController, animated: true, completion: nil)
    }
}
