//
//  MovieDetailsView.swift
//  MovieThings
//
//  Created by Chris Chen on 2019/3/17.
//  Copyright © 2019 Dev4App. All rights reserved.
//

import UIKit

class MovieDetailsView: UIView {
    
    var scrollView = UIScrollView()
    var contentView = UIView()
    
    let posterImageView = UIImageView()
    let synopsisLabel = UILabel()
    let genresLabel = UILabel()
    let genresDetailsLabel = UILabel()
    let languageLabel = UILabel()
    let languageDetailsLabel = UILabel()
    let durationLabel = UILabel()
    let durationDetailsLabel = UILabel()
    let bookingButton = UIButton()
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        // scrollView
        scrollView.backgroundColor = UIColor.theme.lightGray
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.isScrollEnabled = true
        addSubview(scrollView)
        
        scrollView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
        }
        
        // contentView
        contentView.backgroundColor = .white
        contentView.layer.cornerRadius = 8
        scrollView.addSubview(contentView)
        
        contentView.snp.makeConstraints { (make) in
            make.width.equalTo(scrollView).offset(-18)
            make.top.equalTo(scrollView).offset(8)
            make.bottom.equalTo(scrollView).offset(-8)
            make.leading.equalTo(scrollView).offset(8)
            make.trailing.equalTo(scrollView).offset(-8)
        }
        addContentSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Helper functions
    
    func addContentSubviews() {
        
        // posterImageView
        posterImageView.frame = CGRect(x: 0, y: 0, width: 278, height: 413)
        posterImageView.contentMode = .scaleAspectFit
        
        contentView.addSubview(posterImageView)
        
        // synopsisLabel
        synopsisLabel.numberOfLines = 0
        contentView.addSubview(synopsisLabel)
        
        // genresLabel
        contentView.addSubview(genresLabel)
        
        // genresDetailsLabel
        genresDetailsLabel.numberOfLines = 0
        contentView.addSubview(genresDetailsLabel)
        
        // languageLabel
        contentView.addSubview(languageLabel)
        
        // languageDetailsLabel
        contentView.addSubview(languageDetailsLabel)
        
        // durationLabel
        contentView.addSubview(durationLabel)
        
        // durationDetailsLabel
        contentView.addSubview(durationDetailsLabel)
        
        addContentSubviewConstraints()
    }
    
    func addContentSubviewConstraints() {
        
        // posterImageView
        posterImageView.snp.makeConstraints { (make) in
            make.height.equalTo(278)
            make.width.equalTo(413)
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(contentView.snp.top).offset(10)
        }
        
        // synopsisLabel
        synopsisLabel.snp.makeConstraints { (make) in
            make.top.equalTo(posterImageView.snp.bottom).offset(20)
            make.leading.equalTo(contentView.snp.leading).offset(15)
            make.trailing.equalTo(contentView.snp.trailing).offset(-15)
        }
        
        // genresLabel
        genresLabel.snp.makeConstraints { (make) in
            make.width.equalTo(85)
            make.top.equalTo(synopsisLabel.snp.bottom).offset(20)
            make.leading.equalTo(contentView.snp.leading).offset(15)
        }
        
        // genresDetailsLabel
        genresDetailsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(synopsisLabel.snp.bottom).offset(20)
            make.leading.equalTo(genresLabel.snp.trailing).offset(5)
            make.trailing.equalTo(contentView.snp.trailing).offset(-15)
        }
        
        // languageLabel
        languageLabel.snp.makeConstraints { (make) in
            make.width.equalTo(85)
            make.top.equalTo(genresDetailsLabel.snp.bottom).offset(5)
            make.leading.equalTo(contentView.snp.leading).offset(15)
        }
        
        // languageDetailsLabel
        languageDetailsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(genresDetailsLabel.snp.bottom).offset(5)
            make.leading.equalTo(languageLabel.snp.trailing).offset(5)
            make.trailing.equalTo(contentView.snp.trailing).offset(-15)
        }
        
        // durationLabel
        durationLabel.snp.makeConstraints { (make) in
            make.width.equalTo(85)
            make.top.equalTo(languageDetailsLabel.snp.bottom).offset(5)
            make.leading.equalTo(contentView.snp.leading).offset(15)
        }
        
        // durationDetailsLabel
        durationDetailsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(languageDetailsLabel.snp.bottom).offset(5)
            make.bottom.equalTo(contentView.snp.bottom).offset(-15)
            make.leading.equalTo(durationLabel.snp.trailing).offset(5)
            make.trailing.equalTo(contentView.snp.trailing).offset(-15)
        }
    }
    
    func show(withMovieInfo movie: MovieInfo) {
        
        posterImageView.image = movie.image
        synopsisLabel.text = movie.overview
        
        genresLabel.text = "Genres: "
        if let genres = movie.genres {
            var genresString = ""
            for g in genres {
                if g.id != genres.first!.id {
                    genresString += ", "
                }
                genresString += g.name!
            }
            
            if genresString == "" {
                genresString = "None"
            }
            
            genresDetailsLabel.text = genresString
        }
        
        languageLabel.text = "Language: "
        languageDetailsLabel.text = movie.original_language
        
        durationLabel.text = "Duration: "
        if let runtime = movie.runtime {
            durationDetailsLabel.text = String(runtime) + " mins"
        } else {
            durationDetailsLabel.text = "None"
        }
    }
}

