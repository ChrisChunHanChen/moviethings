//
//  MoviewCollectionViewCell.swift
//  MovieThings
//
//  Created by Chris Chen on 2019/3/17.
//  Copyright © 2019 Dev4App. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    var contentSubview = UIView()
    
    let posterImageView = UIImageView()
    let titleLabel = UILabel()
    let popularityLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.backgroundColor = UIColor.theme.lightGray
        
        // ContentSubview
        contentSubview.backgroundColor = .white
        contentSubview.layer.cornerRadius = 5
        contentView.addSubview(contentSubview)
        
        contentSubview.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(5)
            make.bottom.equalToSuperview().offset(-5)
            make.leading.equalToSuperview().offset(5)
            make.trailing.equalToSuperview().offset(-5)
        }
        
        addContentSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - helper functions
    
    func addContentSubviews() {
        // posterImageView
        posterImageView.frame = CGRect(x: 0, y: 0, width: 185, height: 275)
        posterImageView.contentMode = .scaleAspectFit
        contentSubview.addSubview(posterImageView)
        
        // titleLabel
        titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
        titleLabel.numberOfLines = 0
        contentSubview.addSubview(titleLabel)
        
        // popularityLabel
        popularityLabel.font = UIFont.systemFont(ofSize: 14)
        contentSubview.addSubview(popularityLabel)
        
        addContentSubviewConstraints()
    }
    
    func addContentSubviewConstraints() {
        
        // posterImageView
        posterImageView.snp.makeConstraints { (make) in
            make.top.equalTo(contentSubview.snp.top).offset(5)
            make.leading.equalTo(contentSubview.snp.leading).offset(5)
            make.trailing.equalTo(contentSubview.snp.trailing).offset(-5)
        }
        
        // titleLabel
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(posterImageView.snp.bottom).offset(5)
            make.leading.equalTo(contentSubview.snp.leading).offset(5)
            make.trailing.equalTo(contentSubview.snp.trailing).offset(-5)
        }
        
        // popularityLabel
        popularityLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(5)
            make.bottom.equalTo(contentSubview.snp.bottom).offset(-10)
            make.leading.equalTo(contentSubview.snp.leading).offset(5)
            make.trailing.equalTo(contentSubview.snp.trailing).offset(-5)
        }
    }
    
    func show(withMovieInfo movie: MovieInfo) {
        posterImageView.image = movie.image
        titleLabel.text = movie.title
        popularityLabel.text = "Popularity: " + String(movie.popularity!)
    }
}
