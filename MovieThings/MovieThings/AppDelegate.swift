//
//  AppDelegate.swift
//  MovieThings
//
//  Created by Chris Chen on 2019/3/17.
//  Copyright © 2019 Dev4App. All rights reserved.
//

import UIKit
import SVProgressHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupApplicationAppearances()
        setupApplicationViewControllers()
        return true
    }
    
    func setupApplicationViewControllers() {
        
        let movieListViewController = MovieListViewController()
        let movieListNavigationController = UINavigationController(rootViewController: movieListViewController)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = movieListNavigationController
        window?.makeKeyAndVisible()
    }
    
    func setupApplicationAppearances() {
        
        // Window
        let window = UIWindow.appearance()
        window.tintColor = .white
        
        // NavigationBar
        let navigationBar = UINavigationBar.appearance()
        navigationBar.backgroundColor = UIColor.theme.mainTheme
        navigationBar.barTintColor = UIColor.theme.mainTheme
        navigationBar.tintColor = .white
        
        let navBarFont = UIFont.boldSystemFont(ofSize: 19)
        let navBarTintColor = UIColor.theme.darkGreen
        
        navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navBarFont,
                                             NSAttributedString.Key.foregroundColor: navBarTintColor]
        
        // SVProgress
        SVProgressHUD.setDefaultStyle(.dark)
    }
}

