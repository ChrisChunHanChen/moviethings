//
//  ThemeManager.swift
//  MovieThings
//
//  Created by Chris Chen on 2019/3/17.
//  Copyright © 2019 Dev4App. All rights reserved.
//

import UIKit

extension UIColor {
    struct theme {
        static let mainTheme = UIColor(displayP3Red: 0.416, green: 0.867, blue: 0.788, alpha: 1.0)
        static let darkGreen = UIColor(displayP3Red: 0.094, green: 0.322, blue: 0.157, alpha: 1.0)
        static let lightGray = UIColor(red: 0.950, green: 0.950, blue: 0.950, alpha: 1.00)
    }
}
