//
//  WebManager.swift
//  MovieThings
//
//  Created by Chris Chen on 2019/3/17.
//  Copyright © 2019 Dev4App. All rights reserved.
//

import Gloss
import Alamofire
import AlamofireImage
import Alamofire_Gloss

let APIKey = "8ad59e5521127fa75029091e137cec97"

class WebManager {
    
    static let sharedInstance = WebManager()
    
    static func loadMovieList(withPageNum pageNum: Int,
                              failure: @escaping ((Error) -> ()),
                              completion: @escaping (MovieList) -> ()) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let today = dateFormatter.string(from: Date())
        
        let urlString = "https://api.themoviedb.org/3/discover/movie?api_key=\(APIKey)&region=TW&sort_by=release_date.desc&include_adult=false&include_video=false&page=\(pageNum)&release_date.lte=\(today)"
        
        
        var request = URLRequest(url: URL(string: urlString)!)
        request.httpMethod = "GET"
        
        Alamofire.request(request).responseObject(MovieList.self, completionHandler: { (response) in
            switch response.result {
            case .success(let movieList):
                completion(movieList)
            case .failure(let error):
                failure(error)
            }
        })
    }
    
    static func fetchMovieInfo(withMovieID movieID: Int,
                               failure: @escaping ((Error) -> ()),
                               completion: @escaping (MovieInfo) -> ()) {
        
        let urlString = "https://api.themoviedb.org/3/movie/\(movieID)?api_key=\(APIKey)"
        
        var request = URLRequest(url: URL(string: urlString)!)
        request.httpMethod = "GET"
        
        
        Alamofire.request(request).responseObject(MovieInfo.self, completionHandler: { (response) in
            switch response.result {
            case .success(let movieData):
                completion(movieData)
            case .failure(let error):
                failure(error)
            }
        })
    }
    
    
    static func loadImage(forMovie movie: MovieInfo,
                          failure: @escaping ((UIImage?) -> ()),
                          completion: @escaping (UIImage) -> ()) {
        
        if let image = movie.image {
            completion(image)
        }
        
        var urlString = ""
        
        if let imageURL = movie.poster_path ?? movie.backdrop_path {
            urlString = "http://image.tmdb.org/t/p/w185\(imageURL)"
            
            Alamofire.request(urlString).responseImage { response in
                if let image = response.result.value {
                    completion(image)
                }
            }
        }
        
        let image = UIImage(named: "noImage")
        failure(image)
    }
}
