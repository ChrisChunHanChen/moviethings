//
//  MovieInfo.swift
//  MovieThings
//
//  Created by Chris Chen on 2019/3/17.
//  Copyright © 2019 Dev4App. All rights reserved.
//

import Foundation
import Gloss

class MovieList: Glossy {
    
    let results: [MovieInfo]
    
    required init?(json: JSON) {
        guard
            let results: [MovieInfo] = "results" <~~ json
            else {
                return nil
        }
        
        self.results = results
    }
    
    func toJSON() -> JSON? {
        return jsonify([])
    }
}

class MovieInfo: Glossy {
    
    let id: Int?
    let title: String
    let popularity: Double?
    let backdrop_path: String?
    let poster_path: String?
    let overview: String
    var genres: [Genres]?
    let original_language: String
    var image: UIImage?
    var runtime: Int?
    
    required init?(json: JSON) {
        guard
            let title: String = "title" <~~ json,
            let overview: String = "overview" <~~ json,
            let original_language: String = "original_language" <~~ json else {
                return nil
        }
        
        self.id = "id" <~~ json
        self.title = title
        self.popularity = "popularity" <~~ json
        self.backdrop_path = "backdrop_path" <~~ json
        self.poster_path = "poster_path" <~~ json
        self.overview = overview
        self.genres = "genres" <~~ json
        self.original_language = original_language
        self.runtime = "runtime" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([])
    }
}

struct Genres: Glossy {
    
    let id: Int?
    let name: String?
    
    init?(json: JSON) {
        
        self.id = "id" <~~ json
        self.name = "name" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([])
    }
}

